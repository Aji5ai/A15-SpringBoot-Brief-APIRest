package com.brief.API.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.brief.API.model.BookModel;

@Repository
public interface BookRepository extends JpaRepository<BookModel, Long> {
    public List<BookModel> findByTitleContaining(String title); // List pour permettre d'afficher plusieurs livres si plusieurs on le même titre ou bout de titre. Pareil, Containing permet de chercher sans avoir besoin du nom exact
}
