package com.brief.API.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.brief.API.model.TestModel;

@Repository // @Repository est une annotation Spring pour indiquer que la classe est un bean, et que son rôle est de communiquer avec une source de données (en l'occurrence la base de données).
public interface TestRepository extends JpaRepository<TestModel, Long> { // Cette interface étend JpaRepository, qui est une interface fournie par Spring Data JPA. Elle fournit des méthodes CRUD (Create, Read, Update, Delete) pour l'entité TestModel, ainsi que des méthodes de pagination, de tri, etc.
// Elle utilise la généricité pour que son code soit applicable à n’importe quelle entité, d’où la syntaxe “JpaRepository<TestModel, Long>” 
    
}
