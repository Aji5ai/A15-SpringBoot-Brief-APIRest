package com.brief.API.controller;

import java.util.List;
import java.util.Optional;
//Avec AutoWired
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.brief.API.model.BookModel;
//Avec AutoWired
//import com.brief.API.repository.BookRepository;
import com.brief.API.service.BookService;

@RestController
public class BookController {
    // Avec AutoWired
    // @Autowired
    // private BookService bookService;

    // Sans AutoWired
    private BookService bookService;
    public BookController(BookService bookServiceInjected) {
        this.bookService = bookServiceInjected;
    }

    // Puis les méthodes
    @GetMapping("/books")
    public List<BookModel> getAllBooks() {
        return bookService.getAllBooks();
    }

    @GetMapping("/books/{id}")
    public BookModel getBookById(@PathVariable("id") final Long id) {
        Optional<BookModel> book = bookService.getBookById(id);
        if (book.isPresent()) {
            return book.get();
        } else {
            return null;
        }
    }
    // Attention, ne pas faire deux requetes à méthode identique sur la même url !!
    @GetMapping("/books/search") // requête GET à /books/search?title=VotreTitreIci
    // le responseEntity est optionnel
    public ResponseEntity<List<BookModel>> getBooksByTitle(@RequestParam String title) {
        List<BookModel> books = bookService.getBooksByTitle(title);
        return ResponseEntity.ok(books);
    }

    @PostMapping("/books")
    public BookModel createBook(@RequestBody BookModel bookModel) {
        return bookService.saveBook(bookModel);
    }

    @PutMapping("/books/{id}")
    public BookModel updateBook(@PathVariable("id") final Long id, @RequestBody BookModel bookModel) {
        // On vérifie que le livre à update existe avant de le modifier
        Optional<BookModel> existingBookOptional = bookService.getBookById(id);
        if (existingBookOptional.isPresent()) {
            BookModel existingBook = existingBookOptional.get();
            existingBook.setTitle(bookModel.getTitle());
            existingBook.setDescription(bookModel.getDescription());
            existingBook.setAvailable(bookModel.getAvailable());
            return bookService.saveBook(existingBook);
        } else {
            // Cas où le livre n'existe pas
            return null;
        }
    }

    @DeleteMapping("/books/{id}")
    public void deleteBookModel(@PathVariable("id") final Long id) {
        bookService.deleteBook(id);
    }
}
