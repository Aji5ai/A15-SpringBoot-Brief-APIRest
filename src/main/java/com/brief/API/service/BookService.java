package com.brief.API.service;

import java.util.List;
import java.util.Optional;

// Pour autowired
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brief.API.model.BookModel;
import com.brief.API.repository.BookRepository;

@Service
public class BookService {
    // Avec AutoWired
    // @Autowired
    // private BookRepository bookRepository;

    // Sans AutoWired (recommandé)
    private BookRepository bookRepository;

    public BookService(BookRepository bookRepositoryInjected) {
        this.bookRepository = bookRepositoryInjected;
    }

    public List<BookModel> getAllBooks() {
        return bookRepository.findAll(); //Pas besoin de mettre this. à return this.bookRepository.findAll(). Pas obligatoire en Java tant qu'il n'y a pas de confusion dans la méthode (s'il n'y a pas une autre variable qui s'appelle pareil) 
    }

    public Optional<BookModel> getBookById(Long id) {
        return bookRepository.findById(id);
    }

    public List<BookModel> getBooksByTitle(String title) {
        return bookRepository.findByTitleContaining(title);
    }

    public BookModel saveBook(BookModel bookModel) {
        return bookRepository.save(bookModel);
    }

    public void deleteBook(Long id) {
        bookRepository.deleteById(id);
    }
}
