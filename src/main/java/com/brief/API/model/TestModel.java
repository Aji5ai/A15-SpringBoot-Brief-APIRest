package com.brief.API.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity // indique que la classe TestModel est une entité persistante. En d'autres termes, les instances de cette classe peuvent être stockées dans une base de données.
public class TestModel {
    @Id // spécifie que l'attribut id de la classe est la clé primaire de l'entité. Cela signifie que chaque instance de TestModel sera identifiable de manière unique par la valeur de son attribut id.
    @GeneratedValue(strategy = GenerationType.AUTO) // spécifie que la génération de la valeur de l'identifiant sera gérée automatiquement par le fournisseur de persistance
    private Long id; // représente l'identifiant de l'entité TestModel. Comme il est annoté avec @Id, il sert de clé primaire dans la base de données.

    public TestModel(){} // constructeur par défaut de la classe TestModel. Il est défini sans paramètres et ne fait rien d'autre que d'initialiser l'objet. Si on ne met pas de constructeur explicite dans une classe Java, le compilateur en générera automatiquement un par défaut.

    public Long getId(){return this.id;}

    public void setId(Long id){this.id = id;}
}
